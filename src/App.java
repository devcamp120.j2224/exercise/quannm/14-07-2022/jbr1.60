public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");

        Date date1 = new Date(11, 8, 2021);
        Date date2 = new Date(3, 6, 2022);

        System.out.println(date1);
        System.out.println(date2);
    }
}
